#!/bin/bash

# dowork.sh
# 调用方法和参数总结
# 该脚本被multi.sh调用执行，传递给脚本五个参数
# param 1: PID of multi.sh
# param 2: slot number
# param 3: lock file name
# param 4: info file name
# param 5: param file name
# write by xingming on 2012年11月8日11:39:42

# 取出参数文件的全路径信息
logfilepath=`cat $5`                    # 日志文件名称的完全路径

# 先对文件进行解压缩
#echo "这句话会被multi.sh输出到终端显示..." > $4
echo "$2 `cat $5`" > $4

# 在这里可以做该脚本需要完成的内容
sleep $((RANDOM % 10))
# ...
# ...
# ...

echo "$5 done" > $4

# 脚本运行完了，在这里将锁文件删除，multi.sh就可以知道该脚本执行完毕了
rm -f $3

# all work done!
