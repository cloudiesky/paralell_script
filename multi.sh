#!    /bin/bash

# multi.sh
# Shell script with MultiProcess
#
# Created by xingming on 12-9-3.

LC_ALL=zh_CN.UTF-8
export LC_ALL

function help()
{
    echo "Usage:"
    echo " $0 proc job_file [slots]"

    exit
}

if [ $# -lt 2 ]; then help; fi
if [ ! -f $2 ]; then help; fi

fJobs=$2;
nJobs=`cat ${fJobs} | wc -l | sed 's, ,,g'`
nSlots=${3:-9}

function jobsInit()
{
    clear
    echo "$1"", ${nSlots} parser"

    idx=1
    while [ $idx -le ${nSlots} ]; do
        tput cup $(($idx + 1)) 0
        printf "\033[01;34mSlot %2d:\033[00m " $idx
        idx=$((idx + 1))
    done
}

function jobsDump()
{
    tput cup $(($1 + 1)) 0
    printf "\033[01;34mSlot %2d:\033[00m `date +%H:%M:%S` " $1

    if [ -f $$_dump_$1.dmp ]; then
        tput el
        echo "`head -n1 $$_dump_$1.dmp`"
    elif [ v"$2" != "v" ]; then
        tput el
        echo "$2"
    fi
}

function jobsInfo()
{
    tput cup 1 0
    tput el
    echo $*
}


jobsInit "Examining ${nJobs} jobs in directory '`pwd`' now"


# Process jobs
cjob=0
finish=0
while [ ${finish} -eq 0 ]; do
    jobsInfo "Processing ${cjob}/${nJobs} jobs..."

    finish=1

    idx=1
    while [ ${idx} -le ${nSlots} ]; do
        if [ -f $$_lock_${idx}.lck ]; then
            finish=0
            jobsDump ${idx}
        else
            rm -f "$$_lock_${idx}.lck" "$$_dump_${idx}.dmp" "$$_parm_${idx}.opt"
            jobsDump ${idx} "Empty."

            if [ ${cjob} -lt ${nJobs} ]; then
                finish=0

                touch "$$_lock_${idx}.lck";
                head -n $((cjob + 1)) ${fJobs} | tail -n1 > "$$_parm_${idx}.opt"

                # Call process
                #  param 1: PID of multi.sh
                #  param 2: slot number
                #  param 3: lock file name
                #  param 4: info file name
                #  param 5: param file name
                $1 $$ ${idx} "$$_lock_${idx}.lck" "$$_dump_${idx}.dmp" "$$_parm_${idx}.opt" $((cjob + 1)) &

                # Dump process info
                jobsDump ${idx} "Process job..."
                cjob=$((cjob + 1))
            fi
        fi

        idx=$((idx + 1))
    done

    sleep 0.1
done

tput cup $((nSlots + 2)) 0
echo "All finished."

rm -f $$_*

# vim:ft=sh:ai:cindent:noet:sm:sw=4:ts=4
